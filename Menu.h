//
// Created by Haleemur Ali on 2017-05-13.
//

#ifndef ASSIGNMENT1_MENU_H
#define ASSIGNMENT1_MENU_H

#include <ostream>
#include "Text.h"

class Menu {

private:
	const Text PROMPT_PREFIX_LITERAL = Text("?-> ");

	Text top_prompt;
	Text bottom_prompt;
	Text top_text;
	Text bottom_text;

	Text *options_list;
	int count;
	int max_list_size;

    void double_capacity();
	bool valid_choice(int);
	void set(const Menu &other);

	Text formatWithNewLine(const Text &t) const;
	Text formatOptions() const;
	Text formatPrompt() const;
	Text formatOptionElement(int ) const;
public:
	Menu();
    Menu(const Menu& other);
    virtual ~Menu();

    Menu& operator=(const Menu& menu);

    void insert(int index, const Text& option);

    void push_back(char* pOption);
    void push_back(const Text& option);
    Text remove(int index);

    int size() const;
    int capacity() const;

    void pop_back();
    Text get(int k);
    Text toString() const;

    int read_option_number();

	friend std::ostream &operator<<(std::ostream &os, const Menu &menu);

	void set_top_prompt(const Text& t);
    void set_bottom_prompt(const Text& t);
	void set_top_message(const Text& m);
    void set_bottom_message(const Text& m);

    void clear_top_prompt();
    void clear_bottom_prompt();
    void clear_top_message();
    void clear_bottom_message();

    bool isEmpty();
};

#endif //ASSIGNMENT1_MENU_H
