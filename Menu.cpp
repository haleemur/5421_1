//
// Created by Haleemur Ali on 2017-05-13.
//
#include <iostream>
#include <sstream>
#include "Menu.h"


// default constructor
// creates an empty menu object with the initial capacity
// to store 1 text object
// the count & max_list_size are initialized to 0 & 1
// respectively.
// the *_text & *_prompt member variables are automatically
// constructed with the default Text constructor
Menu::Menu()
{
    count = 0;
    max_list_size = 1;
    options_list = new Text[max_list_size];
}

// private method used for both copy constructor and assignment
// operator overload:
// copies the member variables over
// i.e. count, max_list_size, *_text & *_prompt member variables
// initializes a new Text array of size other.max_list_size
// and copies over the contents using std::copy
void Menu::set(const Menu &other)
{
    if (&other == this) {
        return;
    }
    count = other.count;
    max_list_size = other.max_list_size;

    options_list = new Text[max_list_size];
    std::copy(other.options_list
              , other.options_list + count
              , options_list);

    top_prompt = other.top_prompt;
    bottom_prompt = other.bottom_prompt;
    top_text = other.top_text;
    bottom_text = other.bottom_text;
}

// copy constructor
Menu::Menu(const Menu &other)
{ set(other); }


// destructor
// frees up the options_list
Menu::~Menu()
{ delete[] options_list; }

// assignment - copies the other menu
// and returns a pointer to this object
Menu &Menu::operator=(const Menu &menu)
{
    set(menu);
    return *this;
}

// inserts a Text element at the provided index into the options_list
// if the provided index not in [0, count], an out of range error is
// thrown.
// there are 3 different sub-routines for inserting elements.
// 1. insertion is at the end of the array. then the insertion is done via
//    the `push_back` method
// 2. insertion is not at the end of the array, and count < max_list_size, then
//    the elements to the right of `index` are shifted to the right, and the
//    text-vlaue of the element at `index` is `set` to the argument `option`
// 3. insertion is not at the end of the array and count == max_list_size, then
//    a temporary array of twice the size is created, elements at the positions
//    0..index-1 are copied to the new array , then the argument `option` is
//    placed at the position `index`, then the subsequent elements are copied
//    over to the right of index.
void Menu::insert(int index, const Text &option)
{
    if(index < 0 || index > count){
        throw std::out_of_range("Insertion Index must be in range [0, count]");

    } else if (count == index) {
        push_back(option);

    } else if (count < max_list_size) {
	    for (int i = count; i > index; i--) {
            options_list[i] = options_list[i-1];
        }
        options_list[index] = option;
        count++;

    } else {

        Text* tmp = new Text[max_list_size*2];
        if (index > 0) {
            std::copy(options_list, options_list+index, tmp);
        }

        tmp[index] = option;
        std::copy(options_list+index, options_list+count, tmp+index+1);
        count++;
        delete[] options_list;
        options_list = tmp;

    }
}


int Menu::size() const
{ return count; }


int Menu::capacity() const
{ return max_list_size; }


void Menu::double_capacity()
{
    max_list_size *= 2;
    Text* new_list = new Text[max_list_size];
    std::copy(options_list, options_list+count, new_list);

    delete[] options_list;
    options_list = new_list;
}


void Menu::push_back(const Text &option)
{
    if (count == max_list_size) {
        double_capacity();
    }
    options_list[count++] = option;
}


void Menu::push_back(char *pOption)
{ Menu::push_back(Text(pOption)); }

// creates a copy of the element to be removed
// shifts subsequent elements to the left
// decrements the counter
// returns the copied element
Text Menu::remove(int index)
{
    if (index < 0 || index >= count) {
        throw std::out_of_range("Index must be in range [0, count) ");
    }
    Text t = options_list[index];

	for (int i=index; i<count-1; i++) {
        options_list[i] = options_list[i+1];
    }
    pop_back();
    return t;
}

// decrements the counter and clears the memory
// associated with the last element
void Menu::pop_back()
{ options_list[--count].set(""); }


Text Menu::get(int k)
{
    if (k < 0 || k >= count) {
        throw std::out_of_range("Index must be in range [0, count)");
    }
    return options_list[k];
}


// If the argument is a non-empty text, then the text is
// returned with a new line appended
Text Menu::formatWithNewLine(const Text &t) const
{
	Text t2;
    if (!t.isEmpty()) {
        t2.set(t);
        t2.append("\n");
    }
    return t2;
}


// each option element is formatted via this
// method. The option index is passed in as an
// argument, and the formatted string is retuned
// which can be displayed to the console
// e.g. option `Pepsi` at index 0 will be rendered
// as `    (1) Pepsi`
// for spaces are used as indentation.
Text Menu::formatOptionElement(int i) const
{
    Text t = Text("    (");
    t.append(std::to_string(i+1).c_str());
    t.append(") ");
    t.append(options_list[i]);
    t.append("\n");
    return t;
}


// calls formatOptionElement on all option elements
// to construct the Menu option string.
//
Text Menu::formatOptions() const
{
    Text t;
    if (count > 0) {
        t.append("\n");
        for (int i=0; i < count; i++) {
            t.append(formatOptionElement(i));
        }
        t.append("\n");
    }
	return t;
}


// returns the input prompt. if a bottom_prompt has been set
// the bottom prompt is appended to the input prompt.
Text Menu::formatPrompt() const
{
	Text t;
    t.append(PROMPT_PREFIX_LITERAL);
    t.append(" ");
    if (!bottom_prompt.isEmpty()) {
        t.append(bottom_prompt);
        t.append(" ");
    }
    return t;
}

// returns a text representation of the menu object
Text Menu::toString() const
{
	Text t;
    t.append(formatWithNewLine(top_text));
    t.append(formatWithNewLine(top_prompt));
    t.append(formatOptions());
	t.append(formatWithNewLine(bottom_text));
    t.append(formatPrompt());
    return t;
}


std::ostream &operator<<(std::ostream &os, const Menu &menu)
{
	os << menu.toString();
    return os;
}


void Menu::set_top_prompt(const Text &t)
{ top_prompt.set(t); }


void Menu::set_bottom_prompt(const Text &t)
{ bottom_prompt.set(t); }


void Menu::set_top_message(const Text &m)
{ top_text.set(m); }


void Menu::set_bottom_message(const Text &m)
{ bottom_text.set(m); }


void Menu::clear_top_prompt()
{ top_prompt.set(""); }


void Menu::clear_bottom_prompt()
{ bottom_prompt.set("");}


void Menu::clear_top_message()
{ top_text.set(""); }


void Menu::clear_bottom_message()
{ bottom_text.set(""); }


bool Menu::isEmpty()
{ return count == 0; }


int Menu::read_option_number()
{
    // do..while loop prompts the user to enter a
    // valid input, and repeats if an invalid choice
    // is entered. Valid choices are integers between
    // 1..count
    // all other integers & invalid integers (e.g. a123)
    // will be rejected.
    int choice;
    do {
        std::cout << toString();

        std::cin >> choice;

        if (std::cin.fail() || !valid_choice(choice)) {
            std::cout << "Invalid choice "
                      << choice
                      << ". It must be in the range [1, "
                      << count << "]" << std::endl;
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        } else {
            return choice;
        }
    } while (std::cin.fail() || !valid_choice(choice));
}

// returns true if choice is between 1 & count or the options_list is empty
bool Menu::valid_choice(int choice)
{
    return (isEmpty() || (choice >= 1 && choice <= count));
}


