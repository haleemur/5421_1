COMP 5421: Assignment 1
Haleemur Ali (40039217)
=======================


compilation instructions (Mac OSX / Linux)
==========================================

NOTE: g++ must be available in the path

browse to the application folder

```
g++ *.cpp -o menuDriver.out
```

run instructions (Mac OSX / Linux)
==================================

```
./menuDriver.out
```

NOTE: I did not run / test the program on Windows

Known Bugs
==========

1. The menu choice does not provide a correct error message for textual inputs
example:

```
Quench your thirst with our fine drinks
Choose your thirst crusher:

    (1) Iced tea with lemon
    (2) Apple juice

Time to obey your thirst!
?->  Enter a drink number:  adfa
Invalid choice 0. It must be in the range [1, 2]
```