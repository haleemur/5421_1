//
// Created by Haleemur Ali on 2017-05-15.
//

#include <iostream>
#include "Text.h"

int demoText()
{
	Text t1("Welcome to C++");
	Text t2;
	Text t3{ t1 };
	std::cout << "t1: " << t1 << std::endl;
	std::cout << "t2: " << t2 << std::endl;
	std::cout << "t3: " << t3 << std::endl;

	t2.set(" Programming");
    return 0;
}

