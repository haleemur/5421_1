//
// Created by Haleemur Ali on 2017-05-13.
//

#ifndef ASSIGNMENT1_TEXT_H
#define ASSIGNMENT1_TEXT_H

#include <ostream>

class Text {

private:
    char* text;


public:

    Text();
    Text(const char*);
    Text(const Text&);
    Text& operator=(const Text&);
    friend std::ostream &operator<<(std::ostream &os, const Text &t);

    void set(const char* t);
    void set(const Text& t);
    void append(char* t);
    void append(const Text& t);
    int length() const;
    bool isEmpty() const;
    virtual ~Text();

};
#endif //ASSIGNMENT1_TEXT_H