#include <iostream>
#include "Text.h"
#include "Menu.h"

int main() {
    Menu menu;

    int choice = menu.read_option_number();
    std::cout << "you entered: " << choice << std::endl;

    menu.push_back("Pepsi");
    choice = menu.read_option_number();
    std::cout << "you entered: " << choice << std::endl;

    menu.push_back("Apple juice");
    menu.push_back("Root beer");
    choice = menu.read_option_number();
    std::cout << "you entered: " << choice << std::endl;

    menu.set_top_prompt("Choose your thirst crusher: ");
    menu.set_bottom_prompt("Enter a drink number: ");
    choice = menu.read_option_number();
    std::cout << "you entered: " << choice << std::endl;

    menu.pop_back();
    menu.insert(1, Text("Iced tea with lemon"));
    choice = menu.read_option_number();
    std::cout << "you entered: " << choice << std::endl;

    menu.remove(0);
    choice = menu.read_option_number();
    std::cout << "you entered: " << choice << std::endl;

    menu.set_top_message("Quench your thirst with our fine drinks");
	menu.set_bottom_message("Time to obey your thirst!");
    choice = menu.read_option_number();
    std::cout << "you entered: " << choice << std::endl;

    menu.pop_back();
    choice = menu.read_option_number();
    std::cout << "you entered: " << choice << std::endl;

	menu.remove(0);
    std::cout << menu << std::endl;


    menu.set_top_message("Who Says You Can't Buy Happiness?");
    menu.clear_bottom_message();
    menu.set_top_prompt("Just Consider Our Seriously Declicious Ice Cream Flavors for Summer ");
    menu.set_bottom_prompt("Enter the number of your Happiness Flavor: ");
    menu.push_back("Bacon Ice cream!");
    menu.push_back("Strawberry ice cream");
    menu.push_back("Vanilla ice cream");
    menu.push_back("Chocolate chip cookie dough ice cream");
    choice = menu.read_option_number();
    std::cout << "you entered: " << choice << std::endl;

    return 0;

}