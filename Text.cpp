//
// Created by Haleemur Ali on 2017-05-13.
//

#include <cstring>
#include <iostream>
#include "Text.h"

Text::Text(const char *t) {
    text = new char[(int) strlen(t) + 1];
    strcpy(text, t);
}

Text::Text(const Text &t)
{ Text(t.text); }

Text::Text()
        : Text((char*)""){}

// Text::set(const char *t) and Text::set(const Text &t)
//      if the length of the new string differs from the existing string
//      then:
//          delete the current string
//          allocate a new string of the correct size (new string's size)
//      copy the new string to the memory location pointed to by ``text``
void Text::set(const char *t)
{
    int len = (int) strlen(t);
    if (len != length()) {
        delete[] text;
        text = new char[len + 1];
    }
    strcpy(text, t);
}

void Text::set(const Text &t)
{
    if (this == &t) {
        return;
    }
	set(t.text);
}

// self-assignment check performed in `Text::set(const Text&)`
Text& Text::operator=(const Text &t)
{
    Text::set(t);
    return *this;
}

// Text::append(char *t) and Text::append(const Text &t)
//      allocate a new char array `A` of length:
//          this.length + length_of_appended_string + 1 (terminal character)
//      copy current string to position 0 of array `A`
//      copy appended string to position `length()` of array A
//      free memory pointed to by pointer text
//      point text to where newText points to.
void Text::append(char *t)
{
    int newLength = length() + (int)strlen(t);
    char *tmp = new char[newLength + 1];
    strcpy(tmp, text);
    strcpy((tmp+length()), t);

	delete[] text;
    text = tmp;
}

void Text::append(const Text &t)
{ append(t.text); }

int Text::length() const
{ return (int)strlen(text); }

bool Text::isEmpty() const
{ return length() == 0; }

Text::~Text()
{ delete[] text; }

std::ostream &operator<<(std::ostream &os, const Text &t)
{
    os << t.text;
    return os;
}
